let students = [];

// Add Student Function
function addStudent(studName){
    students.push(studName);
    console.log(`addStudent('${studName}')`)
    console.log(`${studName} was added to the student's list.`)
}

addStudent("John");
addStudent("Jane");
addStudent("Joe");


// Count Students Function
function countStudents(){
    console.log(`countStudents()`)
    console.log(`There are a total of ${students.length} students enrolled`)
}

countStudents();


// Print Students Function
function printStudents(){
    console.log(`printStudents()`);
    students.sort().forEach((student) => {
        console.log(student);
    })
}

printStudents()


// Find Students Function
function findStudents(findStudent){
    let totalMatch = [];
    let newFindStudent = findStudent[0].toUpperCase() + findStudent.substring(1);
    console.log(`findStudents("${findStudent}");`)
    
    students.forEach(student => {
        let newStudent = student[0].toUpperCase() + student.substring(1);
        if(newStudent === newFindStudent){
            totalMatch.push(findStudent);
        } else if(student.indexOf(newFindStudent) === 0){
            totalMatch.push(newStudent);
        }
    });

    if(totalMatch.length === 1){
        console.log(`${findStudent} is an enrollee`);
    } else if(totalMatch.length > 1){
        console.log(`${totalMatch.join()} are enrolees`);
    } else {
        console.log(`No student found with the name ${findStudent}`);
    }
}

findStudents("Joe");
findStudents("Bill");
findStudents("j");


// Add Section Function
function addSection(letter){
    let newStudents = students.map(student => {
        let sectionStudent = `${student} - section ${letter}`
        return sectionStudent;
    });
    console.log(`addSection("A")`)
    console.log(newStudents);
}

addSection("A");

function removeStudent(stud) {
    let newStud = stud[0].toUpperCase() + stud.substring(1);
    console.log(`removeStudent("joe")`);

    let studIndex = students.indexOf(newStud);
    students.splice(studIndex, 1);

    console.log(`${stud} was removed from the student's list.`);
}

removeStudent("joe");
printStudents();